<?php

namespace Drupal\mail_debugger\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class UsermailDebuggerForm.
 *
 * This class provides user email debugger form.
 *
 * @package Drupal\mail_debugger\Form
 */
class UsermailDebuggerForm extends FormBase {

  /**
   * The Key value storage object.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  protected $defaultsStorage;

  /**
   * The user Storage Object.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $userStorage;

  /**
   * The user Mail config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $userMailConfig;

  /**
   * UsermailDebuggerForm constructor.
   *
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $storageFactory
   *   The storage Factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity Type Manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config Factory.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(
    PrivateTempStoreFactory $storageFactory,
    EntityTypeManagerInterface $entityTypeManager,
    ConfigFactoryInterface $configFactory
  ) {
    $this->defaultsStorage = $storageFactory->get(static::class);
    $this->userStorage = $entityTypeManager->getStorage('user');
    $this->userMailConfig = $configFactory->get('user.mail');
  }

  /**
   * Instantiates a new instance of this class.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The service container this instance should use.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('tempstore.private'),
      $container->get('entity_type.manager'),
      $container->get('config.factory')
    );
  }

  /**
   * Function returns form id.
   */
  public function getFormId() {
    return "usermail_debugger_form";
  }

  /**
   * Build Form function.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    return [
      'user' => [
        '#title' => $this->t("Send for"),
        '#type' => 'entity_autocomplete',
        '#target_type' => 'user',
        '#default_value' => $this->userStorage->load(
          $this->defaultsStorage->get('user') ?? $this->currentUser()->id()
        ),
        '#selection_handler' => 'default:user',
        '#selection_settings' => [
          'include_anonymous' => FALSE,
        ],
        '#required' => TRUE,
      ],
      'operation' => [
        '#title' => $this->t("Subject"),
        '#type' => 'radios',
        '#options' => $this->getOperations(),
        '#default_value' => $this->defaultsStorage->get('operation'),
        '#required' => TRUE,
      ],
      'actions' => [
        '#type' => 'actions',
        'submit' => [
          '#type' => 'submit',
          '#value' => $this->t("Send"),
        ],
      ],
    ];
  }

  /**
   * Function returns operations array.
   *
   * @return array
   *   array of operations.
   */
  protected function getOperations() {
    $result = [];
    foreach ($this->userMailConfig->get() as $maybeOperation => $config) {
      // Operation?
      if (!empty($config['subject'])) {
        // Tokens intentionally unprocessed.
        $result[$maybeOperation] = $config['subject'];
      }
    }
    return $result;
  }

  /**
   * Submit Form Handler.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->defaultsStorage->set('user', $form_state->getValue('user'));
    $this->defaultsStorage->set('operation',
      $form_state->getValue('operation'));

    /** @var \Drupal\user\Entity\User $user */
    $user = $this->userStorage->load($form_state->getValue('user'));
    $result = _user_mail_notify(
      $form_state->getValue('operation'),
      $user
    );

    if ($result) {
      $this->messenger()->addStatus($this->t("Sent a message to :mail.", [
        ':mail' => $user->getEmail(),
      ]));
    }
  }

}
